from collections import ChainMap

import pytest

RESOURCE_NAME = "my-resource-sub"
SUBSCRIPTION_ID = "subscription_id"
ALIAS = "subscription_test_alias"
BILLING_SCOPE = "TEST_BILLING_SCOPE"
DISPLAY_NAME = "TEST_SUBSCRIPTION_NAME"
WORKLOAD = "PRODUCTION"

RESOURCE_PARAMETERS = {
    "alias": ALIAS,
    "display_name": DISPLAY_NAME,
    "subscription_id": SUBSCRIPTION_ID,
}

RESOURCE_DESCRIBE_PARAMETERS = {
    "alias": None,
    "display_name": DISPLAY_NAME,
    "subscription_id": SUBSCRIPTION_ID,
}


@pytest.mark.asyncio
async def test_present_subscription_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of resource group. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.subscription.subscriptions.present = (
        hub.states.azure.subscription.subscriptions.present
    )
    mock_hub.tool.azure.subscription.subscriptions.convert_raw_subscription_to_present = (
        hub.tool.azure.subscription.subscriptions.convert_raw_subscription_to_present
    )
    mock_hub.tool.azure.subscription.subscriptions.convert_present_to_raw_subscription = (
        hub.tool.azure.subscription.subscriptions.convert_present_to_raw_subscription
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }
    expected_put = {
        "ret": {
            "id": f"/providers/Microsoft.Subscription/aliases/{ALIAS}",
            "name": RESOURCE_NAME,
            "type": "Microsoft.Subscription/aliases",
            "properties": {
                "subscriptionId": SUBSCRIPTION_ID,
            },
        },
        "result": True,
        "status": 200,
        "comment": "Created azure.subscription.subscriptions",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ALIAS in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ALIAS in url
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    ret = await mock_hub.states.azure.subscription.subscriptions.present(
        ctx,
        RESOURCE_NAME,
        ALIAS,
        BILLING_SCOPE,
        DISPLAY_NAME,
        WORKLOAD,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert (
        f"Created azure.subscription.subscriptions '{RESOURCE_NAME}'" in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
        idem_resource_name=RESOURCE_NAME,
    )


@pytest.mark.asyncio
async def test_present_subscription_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of resource group. When a resource exists, 'present' should update the resource with patchable
     parameters.
    """
    mock_hub.states.azure.subscription.subscriptions.present = (
        hub.states.azure.subscription.subscriptions.present
    )
    mock_hub.tool.azure.subscription.subscriptions.convert_raw_subscription_to_present = (
        hub.tool.azure.subscription.subscriptions.convert_raw_subscription_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    resource_parameters_update = {
        "alias": ALIAS,
        "display_name": "UPDATED_SUB_NAME",
        "subscription_id": SUBSCRIPTION_ID,
    }

    resource_parameters_update_raw = {
        "billing_scope": BILLING_SCOPE,
        "display_name": "UPDATED_SUB_NAME",
        "workload": WORKLOAD,
    }

    expected_get = {
        "ret": {
            "id": f"/providers/Microsoft.Subscription/aliases/{ALIAS}",
            "name": RESOURCE_NAME,
            "alias": ALIAS,
            "properties": {
                "subscriptionId": SUBSCRIPTION_ID,
                "provisioningState": "Succeeded",
            },
            "displayName": DISPLAY_NAME,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }
    expected_put = {
        "ret": {
            "id": f"/providers/Microsoft.Subscription/aliases/{ALIAS}",
            "name": RESOURCE_NAME,
            "alias": ALIAS,
            **resource_parameters_update_raw,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }
    expected_post = {
        "ret": {
            "id": f"/providers/Microsoft.Subscription/aliases/{ALIAS}",
            "name": RESOURCE_NAME,
            "alias": ALIAS,
            "display_name": "UPDATED_SUB_NAME",
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert ALIAS in url
        return expected_put

    mock_hub.exec.request.json.get.return_value = expected_get
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters
    mock_hub.exec.request.json.post.return_value = expected_post

    ret = await mock_hub.states.azure.subscription.subscriptions.present(
        ctx, RESOURCE_NAME, ALIAS, BILLING_SCOPE, "UPDATED_SUB_NAME", WORKLOAD
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
        idem_resource_name=RESOURCE_NAME,
    )


@pytest.mark.asyncio
async def test_absent_subscription_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of resource group. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.subscription.subscriptions.absent = (
        hub.states.azure.subscription.subscriptions.absent
    )
    mock_hub.tool.azure.subscription.subscriptions.convert_raw_subscription_to_present = (
        hub.tool.azure.subscription.subscriptions.convert_raw_subscription_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert ALIAS in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.subscription.subscriptions.absent(
        ctx, RESOURCE_NAME, ALIAS
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"azure.subscriptions.subscription '{RESOURCE_NAME}' already absent"
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_absent_subscription_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of resource group. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.subscription.subscriptions.absent = (
        hub.states.azure.subscription.subscriptions.absent
    )
    mock_hub.tool.azure.subscription.subscriptions.convert_raw_subscription_to_present = (
        hub.tool.azure.subscription.subscriptions.convert_raw_subscription_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    expected_get = {
        "ret": {
            "id": f"/providers/Microsoft.Subscription/aliases/{ALIAS}",
            "name": RESOURCE_NAME,
            "alias": ALIAS,
            "displayName": DISPLAY_NAME,
            "properties": {"subscriptionId": SUBSCRIPTION_ID},
        },
        "result": True,
        "status": 200,
        "comment": "",
    }
    expected_cancel = {
        "ret": {
            "properties": {"subscriptionId": SUBSCRIPTION_ID},
        },
        "result": True,
        "status": 200,
        "comment": "",
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_delete_parameters(_ctx, url, success_codes):
        assert ALIAS in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get
    mock_hub.exec.request.json.post.return_value = expected_cancel
    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters

    ret = await mock_hub.states.azure.subscription.subscriptions.absent(
        ctx,
        RESOURCE_NAME,
        ALIAS,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Deleted azure.subscription.subscriptions '{RESOURCE_NAME}'" in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
        idem_resource_name=RESOURCE_NAME,
    )


@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of resource group.
    """
    mock_hub.states.azure.subscription.subscriptions.describe = (
        hub.states.azure.subscription.subscriptions.describe
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.subscription.subscriptions.convert_raw_subscription_to_present = (
        hub.tool.azure.subscription.subscriptions.convert_raw_subscription_to_present
    )

    resource_id = f"/subscriptions/" + SUBSCRIPTION_ID
    expected_list = {
        "ret": {
            "value": [
                {
                    "id": "/subscriptions/" + SUBSCRIPTION_ID,
                    "name": RESOURCE_NAME,
                    "subscription_id:": SUBSCRIPTION_ID,
                    "displayName": DISPLAY_NAME,
                    "alias": None,
                }
            ]
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = await mock_hub.states.azure.subscription.subscriptions.describe(ctx)

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert "azure.subscription.subscriptions.present" in ret_value.keys()
    described_resource = ret_value.get("azure.subscription.subscriptions.present")
    described_resource_map = dict(ChainMap(*described_resource))
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=RESOURCE_DESCRIBE_PARAMETERS,
        idem_resource_name=resource_id,
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    idem_resource_name,
):
    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert expected_old_state["alias"] == old_state.get("alias")
        assert expected_old_state.get("display_name") == old_state.get("display_name")
        assert expected_old_state.get("subscription_id") == old_state.get(
            "subscription_id"
        )
    if new_state:
        assert idem_resource_name == new_state.get("name")
        assert expected_new_state.get("alias") == new_state.get("alias")
        assert expected_new_state.get("display_name") == new_state.get("display_name")
        assert expected_new_state.get("subscription_id") == new_state.get(
            "subscription_id"
        )
