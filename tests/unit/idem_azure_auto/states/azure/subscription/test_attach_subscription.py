from collections import ChainMap

import pytest

RESOURCE_NAME = "my-resource"
SUBSCRIPTION_ID = "subscription_id"
MANAGEMENT_GROUP_ID = "management_group_id"
RESOURCE_PARAMETERS = {}


@pytest.mark.asyncio
async def test_attach_subscription_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of attach subscription. When management group is not attached to subscription,
     'present' should attach subscription to management group
    """
    mock_hub.states.azure.subscription.attach_subscriptions.present = (
        hub.states.azure.subscription.attach_subscriptions.present
    )
    mock_hub.tool.azure.subscription.subscriptions.convert_raw_attach_subscription_to_present = (
        hub.tool.azure.subscription.subscriptions.convert_raw_attach_subscription_to_present
    )
    # mock_hub.tool.azure.resource_management.subscriptions.convert_present_to_raw_resource_group = (
    #     hub.tool.azure.resource_management.subscriptions.convert_present_to_raw_resource_group
    # )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }
    expected_put = {
        "ret": {
            "id": f"/managementGroups/management_group_id/subscriptions/subscription_id",
            "name": RESOURCE_NAME,
            "management_group_id": "management_group_id",
            "subscription_id:": "subscription_id",
        },
        "result": True,
        "status": 200,
        "comment": "Created azure.subscription.attach_subscriptions",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert MANAGEMENT_GROUP_ID in url
        assert SUBSCRIPTION_ID in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert MANAGEMENT_GROUP_ID in url
        assert SUBSCRIPTION_ID in url
        assert json == RESOURCE_PARAMETERS
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    ret = await mock_hub.states.azure.subscription.attach_subscriptions.present(
        ctx, RESOURCE_NAME, MANAGEMENT_GROUP_ID, SUBSCRIPTION_ID, **RESOURCE_PARAMETERS
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert (
        f"Created azure.subscription.attach_subscriptions '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
        idem_resource_name=RESOURCE_NAME,
    )


@pytest.mark.asyncio
async def test_attach_subscription_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of attach subscription. When a management group already associated with subscription,
     'present' should return success without updating any of the parameters.
    """
    mock_hub.states.azure.subscription.attach_subscriptions.present = (
        hub.states.azure.subscription.attach_subscriptions.present
    )
    mock_hub.tool.azure.subscription.subscriptions.convert_raw_attach_subscription_to_present = (
        hub.tool.azure.subscription.subscriptions.convert_raw_attach_subscription_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    resource_parameters_update = {}

    resource_parameters_update_raw = {}

    expected_get = {
        "ret": {
            "id": f"/managementGroups/management_group_id/subscriptions/subscription_id",
            "name": RESOURCE_NAME,
            "management_group_id": "management_group_id",
            "subscription_id:": "subscription_id",
        },
        "result": True,
        "status": 200,
        "comment": "",
    }
    expected_put = {
        "ret": {
            "id": f"/managementGroups/management_group_id/subscriptions/subscription_id",
            "name": RESOURCE_NAME,
            **resource_parameters_update_raw,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert SUBSCRIPTION_ID in url
        assert MANAGEMENT_GROUP_ID in url
        return expected_put

    mock_hub.exec.request.json.get.return_value = expected_get
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    ret = await mock_hub.states.azure.subscription.attach_subscriptions.present(
        ctx,
        RESOURCE_NAME,
        MANAGEMENT_GROUP_ID,
        SUBSCRIPTION_ID,
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert (
        f"No update required azure.subscription.attach_subscriptions '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=RESOURCE_PARAMETERS,
        idem_resource_name=RESOURCE_NAME,
    )


@pytest.mark.asyncio
async def test_absent_attach_subscription_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of attach subscription. When a management group is not associated with subscription,
     'absent' should just return success.
    """
    mock_hub.states.azure.subscription.attach_subscriptions.absent = (
        hub.states.azure.subscription.attach_subscriptions.absent
    )
    mock_hub.tool.azure.subscription.subscriptions.convert_raw_attach_subscription_to_present = (
        hub.tool.azure.subscription.subscriptions.convert_raw_attach_subscription_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    expected_get = {
        "ret": {},
        "result": False,
        "status": 404,
        "comment": "Not Found.",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert MANAGEMENT_GROUP_ID in url
        assert SUBSCRIPTION_ID in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.subscription.attach_subscriptions.absent(
        ctx, RESOURCE_NAME, MANAGEMENT_GROUP_ID, SUBSCRIPTION_ID
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"azure.subscription.attach_subscriptions '{RESOURCE_NAME}' is not associated with management group"
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_absent_attach_subscription_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of attach subscription. When management group associated with subscription,
     'absent' should de-associate respective subscription from management group.
    """
    mock_hub.states.azure.subscription.attach_subscriptions.absent = (
        hub.states.azure.subscription.attach_subscriptions.absent
    )
    mock_hub.tool.azure.subscription.subscriptions.convert_raw_attach_subscription_to_present = (
        hub.tool.azure.subscription.subscriptions.convert_raw_attach_subscription_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    expected_get = {
        "ret": {
            "id": f"/managementGroups/{MANAGEMENT_GROUP_ID}/subscriptions/{SUBSCRIPTION_ID}",
            "name": RESOURCE_NAME,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_delete_parameters(_ctx, url, success_codes):
        assert SUBSCRIPTION_ID in url
        assert MANAGEMENT_GROUP_ID in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get
    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters

    ret = await mock_hub.states.azure.subscription.attach_subscriptions.absent(
        ctx, RESOURCE_NAME, MANAGEMENT_GROUP_ID, SUBSCRIPTION_ID
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"De-associated azure.subscription.attach_subscriptions '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
        idem_resource_name=RESOURCE_NAME,
    )


@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of subscriptions.
    """
    mock_hub.states.azure.subscription.attach_subscriptions.describe = (
        hub.states.azure.subscription.attach_subscriptions.describe
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.subscription.subscriptions.convert_raw_attach_subscription_to_present = (
        hub.tool.azure.subscription.subscriptions.convert_raw_attach_subscription_to_present
    )

    resource_id = f"/providers/MicrosoftManagement/managementGroups/{MANAGEMENT_GROUP_ID}/subscriptions/{SUBSCRIPTION_ID}"
    expected_list = {
        "ret": {
            "value": [
                {
                    "name": SUBSCRIPTION_ID,
                    "id": "/subscriptions/SUBSCRIPTION_ID",
                    "type": "/subscriptions",
                    "properties": {
                        "tenantId": "1234e10a-143f-1abc-1a09-1a2aa3c4ddd5",
                        "numberOfChildren": 0,
                        "numberOfChildGroups": 0,
                        "numberOfDescendants": 0,
                        "displayName": "idem-fixture-subscription",
                        "parentDisplayNameChain": [
                            "Tenant Root Group",
                            MANAGEMENT_GROUP_ID,
                        ],
                        "parent": {
                            "id": f"/providers/Microsoft.Management/managementGroups/{MANAGEMENT_GROUP_ID}"
                        },
                        "parentNameChain": [
                            "1234e10a-143f-1abc-1a09-1a2aa3c4ddd5",
                            MANAGEMENT_GROUP_ID,
                        ],
                        "inheritedPermissions": "delete",
                        "permissions": "delete",
                    },
                }
            ]
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    mock_hub.exec.request.json.post.return_value = expected_list

    ret = await mock_hub.states.azure.subscription.attach_subscriptions.describe(ctx)

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert "azure.subscription.attach_subscriptions.present" in ret_value.keys()
    described_resource = ret_value.get(
        "azure.subscription.attach_subscriptions.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
        idem_resource_name=resource_id,
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    idem_resource_name,
):
    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert expected_old_state["management_group_id"] == old_state.get(
            "management_group_id"
        )
        assert expected_old_state.get("subscription_id") == old_state.get(
            "subscription_id"
        )
