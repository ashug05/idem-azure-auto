import copy
from collections import ChainMap

import pytest

RESOURCE_NAME = "my-resource"
MANAGEMENT_GROUP_NAME = "my-management-group"
RESOURCE_PARAMETERS = {
    "display_name": "test_management",
    "parent_id": "/providers/Microsoft.Management/managementGroups/parent_management_group",
}
RESOURCE_PARAMETERS_RAW = {
    "properties": {
        "displayName": "test_management",
        "details": {
            "parent": {
                "id": "/providers/Microsoft.Management/managementGroups/parent_management_group"
            }
        },
    }
}


@pytest.mark.asyncio
async def test_present_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of resource group. When a resource does not exist, 'present' should create the resource.
    """
    mock_hub.states.azure.management_groups.management_groups.present = (
        hub.states.azure.management_groups.management_groups.present
    )
    mock_hub.tool.azure.management_groups.management_groups.convert_raw_management_group_to_present = (
        hub.tool.azure.management_groups.management_groups.convert_raw_management_group_to_present
    )
    mock_hub.tool.azure.management_groups.management_groups.convert_present_to_raw_management_group = (
        hub.tool.azure.management_groups.management_groups.convert_present_to_raw_management_group
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    expected_get = {
        "ret": {},
        "result": False,
        "status": 403,
        "comment": "Forbidden",
    }
    expected_put = {
        "ret": {
            "id": f"/providers/Microsoft.Management/managementGroups/{MANAGEMENT_GROUP_NAME}",
            "name": RESOURCE_NAME,
        },
        "result": True,
        "status": 202,
        "comment": "Accepted",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert MANAGEMENT_GROUP_NAME in url
        return expected_get

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert MANAGEMENT_GROUP_NAME in url
        return expected_put

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.management_groups.management_groups.present(
        test_ctx, RESOURCE_NAME, MANAGEMENT_GROUP_NAME, **RESOURCE_PARAMETERS
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Would create azure.management_groups.management_groups '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
        idem_resource_name=RESOURCE_NAME,
    )
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters
    ret = await mock_hub.states.azure.management_groups.management_groups.present(
        ctx, RESOURCE_NAME, MANAGEMENT_GROUP_NAME, **RESOURCE_PARAMETERS
    )
    assert ret["comment"]
    assert not ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Created azure.management_groups.management_groups '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=ret["new_state"],
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
        idem_resource_name=RESOURCE_NAME,
    )


@pytest.mark.asyncio
async def test_present_resource_exists(hub, mock_hub, ctx):
    """
    Test 'present' state of resource group. When a resource exists, 'present' should update the resource with patchable
     parameters.
    """
    mock_hub.states.azure.management_groups.management_groups.present = (
        hub.states.azure.management_groups.management_groups.present
    )
    mock_hub.tool.azure.management_groups.management_groups.convert_raw_management_group_to_present = (
        hub.tool.azure.management_groups.management_groups.convert_raw_management_group_to_present
    )
    mock_hub.tool.azure.management_groups.management_groups.update_management_groups_payload = (
        hub.tool.azure.management_groups.management_groups.update_management_groups_payload
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )

    resource_parameters_update = {
        "display_name": "test_management2",
        "parent_id": "/providers/Microsoft.Management/managementGroups/parent_management_group",
    }
    resource_parameters_update_raw = {
        "properties": {
            "displayName": "test_management2",
            "details": {
                "parent": {
                    "id": "/providers/Microsoft.Management/managementGroups/parent_management_group"
                }
            },
        }
    }

    expected_get = {
        "ret": {
            "id": f"/providers/Microsoft.Management/managementGroups/{MANAGEMENT_GROUP_NAME}",
            "name": RESOURCE_NAME,
            **RESOURCE_PARAMETERS_RAW,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }
    expected_put = {
        "ret": {
            "id": "/providers/Microsoft.Management/managementGroups/{MANAGEMENT_GROUP_NAME}",
            "name": RESOURCE_NAME,
            **resource_parameters_update_raw,
        },
        "result": True,
        "status": 200,
        "comment": "",
    }

    def _check_put_parameters(_ctx, url, success_codes, json):
        assert MANAGEMENT_GROUP_NAME in url
        assert resource_parameters_update_raw["properties"] == json["properties"]
        return expected_put

    mock_hub.exec.request.json.get.return_value = expected_get
    # Test present() with --test flag on
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.management_groups.management_groups.present(
        test_ctx,
        RESOURCE_NAME,
        MANAGEMENT_GROUP_NAME,
        **resource_parameters_update,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert RESOURCE_NAME == ret["old_state"].get("name")
    assert RESOURCE_NAME == ret["new_state"].get("name")
    assert (
        f"Would update azure.management_groups.management_groups '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
        idem_resource_name=RESOURCE_NAME,
    )
    mock_hub.exec.request.json.put.side_effect = _check_put_parameters

    ret = await mock_hub.states.azure.management_groups.management_groups.present(
        ctx, RESOURCE_NAME, MANAGEMENT_GROUP_NAME, **resource_parameters_update
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert ret["new_state"]
    assert (
        f"Updated azure.management_groups.management_groups '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=ret["new_state"],
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=resource_parameters_update,
        idem_resource_name=RESOURCE_NAME,
    )


@pytest.mark.asyncio
async def test_absent_resource_not_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of resource group. When a resource does not exist, 'absent' should just return success.
    """
    mock_hub.states.azure.management_groups.management_groups.absent = (
        hub.states.azure.management_groups.management_groups.absent
    )
    mock_hub.tool.azure.management_groups.management_groups.convert_raw_management_group_to_present = (
        hub.tool.azure.management_groups.management_groups.convert_raw_management_group_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    expected_get = {
        "ret": {},
        "result": False,
        "status": 403,
        "comment": f"azure.management_groups.management_groups '{RESOURCE_NAME}' already absent",
    }

    def _check_get_parameters(_ctx, url, success_codes):
        assert MANAGEMENT_GROUP_NAME in url
        return expected_get

    mock_hub.exec.request.json.get.side_effect = _check_get_parameters

    ret = await mock_hub.states.azure.management_groups.management_groups.absent(
        ctx, RESOURCE_NAME, MANAGEMENT_GROUP_NAME
    )
    assert not ret["result"], ret["comment"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"azure.management_groups.management_groups '{RESOURCE_NAME}' already absent"
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_absent_resource_exists(hub, mock_hub, ctx):
    """
    Test 'absent' state of resource group. When a resource exists, 'absent' should delete the resource.
    """
    mock_hub.states.azure.management_groups.management_groups.absent = (
        hub.states.azure.management_groups.management_groups.absent
    )
    mock_hub.tool.azure.management_groups.management_groups.convert_raw_management_group_to_present = (
        hub.tool.azure.management_groups.management_groups.convert_raw_management_group_to_present
    )
    mock_hub.tool.azure.test_state_utils.generate_test_state = (
        hub.tool.azure.test_state_utils.generate_test_state
    )
    expected_get = {
        "ret": {
            "id": f"/providers/Microsoft.Management/managementGroups/{MANAGEMENT_GROUP_NAME}",
            "name": RESOURCE_NAME,
            "properties": {"displayName": "test_management"},
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }
    expected_delete = {
        "ret": {},
        "result": True,
        "status": 200,
        "comment": "Deleted",
    }

    def _check_delete_parameters(_ctx, url, success_codes):
        assert MANAGEMENT_GROUP_NAME in url
        return expected_delete

    mock_hub.exec.request.json.get.return_value = expected_get
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await mock_hub.states.azure.management_groups.management_groups.absent(
        test_ctx, RESOURCE_NAME, MANAGEMENT_GROUP_NAME
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Would delete azure.management_groups.management_groups '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    assert RESOURCE_NAME == ret["old_state"].get("name")
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
        idem_resource_name=RESOURCE_NAME,
    )

    mock_hub.exec.request.raw.delete.side_effect = _check_delete_parameters

    ret = await mock_hub.states.azure.management_groups.management_groups.absent(
        ctx, RESOURCE_NAME, MANAGEMENT_GROUP_NAME
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"]
    assert not ret["new_state"]
    assert (
        f"Deleted azure.management_groups.management_groups '{RESOURCE_NAME}'"
        in ret["comment"]
    )
    check_returned_states(
        old_state=ret["old_state"],
        new_state=None,
        expected_old_state=RESOURCE_PARAMETERS,
        expected_new_state=None,
        idem_resource_name=RESOURCE_NAME,
    )


@pytest.mark.asyncio
async def test_describe(hub, mock_hub, ctx):
    """
    Test 'describe' state of resource group.
    """
    mock_hub.states.azure.management_groups.management_groups.describe = (
        hub.states.azure.management_groups.management_groups.describe
    )
    mock_hub.tool.azure.request.paginate = hub.tool.azure.request.paginate
    mock_hub.tool.azure.uri.get_parameter_value_in_dict = (
        hub.tool.azure.uri.get_parameter_value_in_dict
    )
    mock_hub.tool.azure.management_groups.management_groups.convert_raw_management_group_to_present = (
        hub.tool.azure.management_groups.management_groups.convert_raw_management_group_to_present
    )
    resource_id = (
        f"/providers/Microsoft.Management/managementGroups/{MANAGEMENT_GROUP_NAME}"
    )
    expected_list = {
        "ret": {
            "value": [
                {"id": resource_id, "name": RESOURCE_NAME, **RESOURCE_PARAMETERS_RAW}
            ]
        },
        "result": True,
        "status": 200,
        "comment": "Found.",
    }

    mock_hub.exec.request.json.get.return_value = expected_list

    ret = await mock_hub.states.azure.management_groups.management_groups.describe(ctx)

    assert resource_id == list(ret.keys())[0]
    ret_value = ret.get(resource_id)
    assert "azure.management_groups.management_groups.present" in ret_value.keys()
    described_resource = ret_value.get(
        "azure.management_groups.management_groups.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert resource_id == described_resource_map.get("name")
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=RESOURCE_PARAMETERS,
        idem_resource_name=resource_id,
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    idem_resource_name,
):
    if old_state is not None:
        assert idem_resource_name == old_state.get("name")
        if old_state.get("display_name") is not None:
            assert expected_old_state["display_name"] == old_state.get("display_name")
        if old_state.get("parent_id") is not None:
            assert expected_old_state["parent_id"] == old_state.get("parent_id")

    if new_state is not None:
        assert idem_resource_name == new_state.get("name")
        if new_state.get("display_name") is not None:
            assert expected_new_state["display_name"] == new_state.get("display_name")
        if new_state.get("parent_id") is not None:
            assert expected_new_state["parent_id"] == new_state.get("parent_id")
