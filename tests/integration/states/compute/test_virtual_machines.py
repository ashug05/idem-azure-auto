import copy
import time
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_virtual_machine(
    hub,
    ctx,
    resource_group_fixture,
    network_interface_fixture,
    network_interface_update_fixture,
):
    """
    This test provisions a virtual machine, describes them and deletes them.
    """

    # Create VM
    vm_name = "idem-test-virtual-machine-" + str(int(time.time()))
    resource_group_name = resource_group_fixture.get("name")
    network_interface_id = network_interface_fixture.get("id")
    network_interface_update_id = network_interface_update_fixture.get("id")
    subscription_id = ctx.acct.subscription_id
    computer_name = "idem-test-compute-" + str(int(time.time()))
    os_disk_name = "idem-test-os-disk-" + str(int(time.time()))
    data_disk_name = "idem-test-data-disk-" + str(int(time.time()))
    vm_parameters = {
        "location": "eastus",
        "virtual_machine_size": "Standard_B1ls",
        "network_interface_ids": [network_interface_id],
        "storage_image_reference": {
            "image_sku": "18.04-LTS",
            "image_publisher": "Canonical",
            "image_version": "latest",
            "image_offer": "UbuntuServer",
        },
        "storage_os_disk": {
            "storage_account_type": "Standard_LRS",
            "disk_name": os_disk_name,
            "disk_caching": "ReadWrite",
            "disk_size_in_GB": 30,
            "disk_create_option": "FromImage",
            "disk_delete_option": "Delete",
        },
        "storage_data_disks": [
            {
                "disk_name": data_disk_name,
                "disk_size_in_GB": 4,
                "disk_logical_unit_number": 0,
                "disk_caching": "ReadWrite",
                "disk_create_option": "Empty",
                "storage_account_type": "Premium_LRS",
                "disk_delete_option": "Delete",
            }
        ],
        "os_profile": {
            "admin_username": "admin123",
            "computer_name": computer_name,
            "admin_password": "admin-password$A",
        },
        "tags": {"tagkey-1": "tagvalue-1", "tagkey-2": "tagvalue-2"},
    }

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Create virtual machine with --test
    vm_ret = await hub.states.azure.compute.virtual_machines.present(
        test_ctx,
        name=vm_name,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        subscription_id=subscription_id,
        **vm_parameters,
    )
    assert vm_ret["result"], vm_ret["comment"]
    assert not vm_ret["old_state"] and vm_ret["new_state"]
    assert (
        f"Would create azure.compute.virtual_machines '{vm_name}'" in vm_ret["comment"]
    )
    check_returned_states(
        old_state=None,
        new_state=vm_ret["new_state"],
        expected_old_state=None,
        expected_new_state=vm_parameters,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        idem_resource_name=vm_name,
    )
    resource_id = vm_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.Compute/virtualMachines/{vm_name}" == resource_id
    )

    # Create virtual machine in real
    vm_ret = await hub.states.azure.compute.virtual_machines.present(
        ctx,
        name=vm_name,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        subscription_id=subscription_id,
        **vm_parameters,
    )
    assert vm_ret["result"], vm_ret["comment"]
    assert not vm_ret["old_state"] and vm_ret["new_state"]
    assert f"Created azure.compute.virtual_machines '{vm_name}'" in vm_ret["comment"]
    check_returned_states(
        old_state=None,
        new_state=vm_ret["new_state"],
        expected_old_state=None,
        expected_new_state=vm_parameters,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        idem_resource_name=vm_name,
    )
    resource_id = vm_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.Compute/virtualMachines/{vm_name}" == resource_id
    )

    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"https://management.azure.com{resource_id}?api-version=2021-07-01",
        retry_count=5,
        retry_period=25,
    )

    # Describe virtual machine
    describe_ret = await hub.states.azure.compute.virtual_machines.describe(ctx)

    resource_group_name_capital = resource_group_name.upper()
    describe_resource_id = (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name_capital}"
        f"/providers/Microsoft.Compute/virtualMachines/{vm_name}"
    )
    assert describe_resource_id in describe_ret
    describe_resource = describe_ret.get(describe_resource_id)
    described_resource = describe_resource.get("azure.compute.virtual_machines.present")
    described_resource_map = dict(ChainMap(*described_resource))
    check_returned_states(
        old_state=None,
        new_state=described_resource_map,
        expected_old_state=None,
        expected_new_state=described_resource_map,
        virtual_machine_name=vm_name,
        resource_group_name=resource_group_name_capital,
        idem_resource_name=describe_resource_id,
    )

    vm_update_parameters = {
        "location": "eastus",
        "network_interface_ids": [network_interface_update_id],
        "virtual_machine_size": "Standard_B1s",
        "os_profile": {
            "admin_username": "admin123",
            "computer_name": computer_name,
            "admin_password": "admin-password$A",
        },
        "storage_image_reference": {
            "image_sku": "18.04-LTS",
            "image_publisher": "Canonical",
            "image_version": "latest",
            "image_offer": "UbuntuServer",
        },
        "storage_os_disk": {
            "storage_account_type": "Standard_LRS",
            "disk_name": os_disk_name,
            "disk_caching": "ReadWrite",
            "disk_size_in_GB": 30,
            "disk_create_option": "FromImage",
            "disk_delete_option": "Detach",
        },
        "storage_data_disks": [
            {
                "disk_name": data_disk_name,
                "disk_size_in_GB": 4,
                "disk_logical_unit_number": 0,
                "disk_caching": "ReadWrite",
                "disk_create_option": "Empty",
                "storage_account_type": "Premium_LRS",
                "disk_delete_option": "Detach",
            }
        ],
        "tags": {"tag-new-key": "tag-new-value"},
    }

    # Update virtual machine with --test
    vm_ret = await hub.states.azure.compute.virtual_machines.present(
        test_ctx,
        name=vm_name,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        subscription_id=subscription_id,
        **vm_update_parameters,
    )
    assert vm_ret["result"], vm_ret["comment"]
    assert vm_ret["old_state"] and vm_ret["new_state"]
    assert (
        f"Would update azure.compute.virtual_machines '{vm_name}'" in vm_ret["comment"]
    )
    check_returned_states(
        old_state=vm_ret["old_state"],
        new_state=vm_ret["new_state"],
        expected_old_state=vm_parameters,
        expected_new_state=vm_update_parameters,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        idem_resource_name=vm_name,
    )
    resource_id = vm_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.Compute/virtualMachines/{vm_name}" == resource_id
    )

    # Update virtual machine in real
    vm_ret = await hub.states.azure.compute.virtual_machines.present(
        ctx,
        name=vm_name,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        subscription_id=subscription_id,
        **vm_update_parameters,
    )
    assert vm_ret["result"], vm_ret["comment"]
    assert vm_ret["old_state"] and vm_ret["new_state"]
    assert f"Updated azure.compute.virtual_machines '{vm_name}'" in vm_ret["comment"]
    check_returned_states(
        old_state=vm_ret["old_state"],
        new_state=vm_ret["new_state"],
        expected_old_state=vm_parameters,
        expected_new_state=vm_update_parameters,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        idem_resource_name=vm_name,
    )
    resource_id = vm_ret["new_state"].get("resource_id")
    assert (
        f"/subscriptions/{ctx.acct.subscription_id}/resourceGroups/{resource_group_name}"
        f"/providers/Microsoft.Compute/virtualMachines/{vm_name}" == resource_id
    )
    await hub.tool.azure.resource.wait_for_present(
        ctx,
        url=f"https://management.azure.com{resource_id}?api-version=2021-07-01",
        retry_count=5,
        retry_period=25,
    )

    # Delete virtual machine with --test
    vm_del_ret = await hub.states.azure.compute.virtual_machines.absent(
        test_ctx,
        name=vm_name,
        subscription_id=subscription_id,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
    )
    assert vm_del_ret["result"], vm_del_ret["comment"]
    assert vm_del_ret["old_state"] and not vm_del_ret["new_state"]
    assert (
        f"Would delete azure.compute.virtual_machines '{vm_name}'"
        in vm_del_ret["comment"]
    )
    check_returned_states(
        old_state=vm_del_ret["old_state"],
        new_state=None,
        expected_old_state=vm_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        idem_resource_name=vm_name,
    )

    # Delete virtual machine in real
    vm_del_ret = await hub.states.azure.compute.virtual_machines.absent(
        ctx,
        name=vm_name,
        subscription_id=subscription_id,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
    )
    assert vm_del_ret["result"], vm_del_ret["comment"]
    assert vm_del_ret["old_state"] and not vm_del_ret["new_state"]

    assert (
        f"Deleted azure.compute.virtual_machines '{vm_name}'" in vm_del_ret["comment"]
    )
    check_returned_states(
        old_state=vm_del_ret["old_state"],
        new_state=None,
        expected_old_state=vm_update_parameters,
        expected_new_state=None,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
        idem_resource_name=vm_name,
    )

    await hub.tool.azure.resource.wait_for_absent(
        ctx,
        url=f"https://management.azure.com{resource_id}?api-version=2021-07-01",
        retry_count=10,
        retry_period=25,
    )
    # Delete virtual machine again
    vm_del_ret = await hub.states.azure.compute.virtual_machines.absent(
        ctx,
        name=vm_name,
        subscription_id=subscription_id,
        resource_group_name=resource_group_name,
        virtual_machine_name=vm_name,
    )
    assert vm_del_ret["result"], vm_del_ret["comment"]
    assert not vm_del_ret["old_state"] and not vm_del_ret["new_state"]
    assert (
        f"azure.compute.virtual_machines '{vm_name}' already absent"
        in vm_del_ret["comment"]
    )


def check_returned_states(
    old_state,
    new_state,
    expected_old_state,
    expected_new_state,
    resource_group_name,
    virtual_machine_name,
    idem_resource_name,
):
    if old_state:
        assert idem_resource_name == old_state.get("name")
        assert resource_group_name == old_state.get("resource_group_name")
        assert virtual_machine_name == old_state.get("virtual_machine_name")
        assert expected_old_state["virtual_machine_size"] == old_state.get(
            "virtual_machine_size"
        )
        assert expected_old_state["location"] == old_state.get("location")
        assert expected_old_state["network_interface_ids"] == old_state.get(
            "network_interface_ids"
        )
        assert expected_old_state["storage_image_reference"] == old_state.get(
            "storage_image_reference"
        )
        assert expected_old_state["tags"] == old_state.get("tags")

        expected_old_state_copy = copy.deepcopy(expected_old_state)
        old_state_copy = copy.deepcopy(old_state)
        expected_old_state_copy["os_profile"]["admin_password"] = None
        old_state_copy["os_profile"]["admin_password"] = None
        assert expected_old_state_copy["os_profile"] == old_state_copy.get("os_profile")

        expected_old_state_copy["storage_os_disk"]["disk_id"] = None
        old_state_copy["storage_os_disk"]["disk_id"] = None
        assert expected_old_state_copy["storage_os_disk"] == old_state_copy.get(
            "storage_os_disk"
        )

        expected_old_state_copy["storage_data_disks"][0]["disk_id"] = None
        old_state_copy["storage_data_disks"][0]["disk_id"] = None
        assert expected_old_state_copy["storage_data_disks"] == old_state_copy.get(
            "storage_data_disks"
        )

    if new_state:
        assert idem_resource_name == new_state.get("name")
        assert resource_group_name == new_state.get("resource_group_name")
        assert virtual_machine_name == new_state.get("virtual_machine_name")
        assert expected_new_state["virtual_machine_size"] == new_state.get(
            "virtual_machine_size"
        )
        assert expected_new_state["location"] == new_state.get("location")
        assert expected_new_state["network_interface_ids"] == new_state.get(
            "network_interface_ids"
        )
        assert expected_new_state["storage_image_reference"] == new_state.get(
            "storage_image_reference"
        )
        assert expected_new_state["tags"] == new_state.get("tags")

        expected_new_state_copy = copy.deepcopy(expected_new_state)
        new_state_copy = copy.deepcopy(new_state)
        expected_new_state_copy["os_profile"]["admin_password"] = None
        new_state_copy["os_profile"]["admin_password"] = None
        assert expected_new_state_copy["os_profile"] == new_state_copy.get("os_profile")

        expected_new_state_copy["storage_os_disk"]["disk_id"] = None
        new_state_copy["storage_os_disk"]["disk_id"] = None
        assert expected_new_state_copy["storage_os_disk"] == new_state_copy.get(
            "storage_os_disk"
        )

        expected_new_state_copy["storage_data_disks"][0]["disk_id"] = None
        new_state_copy["storage_data_disks"][0]["disk_id"] = None
        assert expected_new_state_copy["storage_data_disks"] == new_state_copy.get(
            "storage_data_disks"
        )
